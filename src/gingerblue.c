/* $Id$

   Copyright (C) 2020-2022 Aamot Software
   Author(s): Ole Aamot <ole@gnome.org>
   License: GNU GPL version 3
   Version: 6.2.0 (2022-07-09)
   Website: http://www.gingerblue.org/

 */

#include <glib/gstdio.h>
#include <glib/gi18n.h>
#include <gst/gst.h>
#include <gtk/gtk.h>
#include "gingerblue.h"
#include "gingerblue-file.h"

gint
gb_exit (void) {
	gst_deinit();
	gtk_main_quit();
}

void
gb_window_break_record (GtkButton *record, GtkVolumeButton *volume) {
	/* gtk_button_set_label(GTK_BUTTON (cue), "Continue Recording"); */
	/* g_signal_connect (GTK_BUTTON (cue), "clicked", G_CALLBACK (gb_window_new_record), gingerblue_data->volume); */
}

void
gb_window_pause_record (GtkButton *record, GtkVolumeButton *volume) {
	/* gtk_button_set_label(GTK_BUTTON (cue), "Continue Recording"); */
	/* g_signal_connect (GTK_BUTTON (cue), "clicked", G_CALLBACK (gb_window_new_record), gingerblue_data->volume); */
}

GingerblueData *
gb_window_new_record (GtkButton *record, GtkVolumeButton *volume) {
	/* gtk_button_set_label(GTK_BUTTON (record), "Stop Recording"); */
}

GingerblueData *
gb_window_store_volume (GtkButton *record, GtkVolumeButton *volume) {
	/* gtk_button_set_label(GTK_BUTTON (record), "Stop Recording"); */
}

gdouble
gb_window_set_volume (GtkVolumeButton *volume, gdouble value) {
	gtk_scale_button_set_value (GTK_SCALE_BUTTON (volume), (gdouble) value);
}

gdouble
gb_window_new_volume (GtkVolumeButton *volume, gchar *msg) {
	g_print ("New volume: %0.2f\n", (gdouble) gtk_scale_button_get_value (GTK_SCALE_BUTTON (volume)));
	return (gdouble) gtk_scale_button_get_value (GTK_SCALE_BUTTON (volume));
}

gdouble
gb_window_get_volume (GtkVolumeButton *volume) {
	return (gdouble) gtk_scale_button_get_value (GTK_SCALE_BUTTON (volume));
}
