/* $Id$

   Copyright (C) 2020-2022 Aamot Software
   Author(s): Ole Aamot <ole@gnome.org>
   License: GNU GPL version 3
   Version: 6.2.0 (2022-07-09)
   Website: http://www.gingerblue.org/

 */

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <gst/gst.h>
#include "gingerblue.h"
#include "gingerblue-config.h"
#include "gingerblue-main.h"
#include "gingerblue-main-loop.h"
#include "gingerblue-studio-config.h"

int main_app (gint argc, gchar *argv[]) {
	GingerblueData *gingerblue_config;
        GtkWindow *gingerblue_window;
	gtk_init (&argc, &argv);
        gingerblue_config = main_config (gingerblue_window, "studios.gingerblue.org");
	gingerblue_window = gingerblue_main_loop (gingerblue_config);
	gtk_widget_show_all (gingerblue_window);
	gst_init(&argc, &argc);
	gtk_main();	
	return (0);
}
