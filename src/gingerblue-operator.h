/* $Id$

   Copyright (C) 2020-2021 Aamot Software
   Author(s): Ole Aamot <ole@gnome.org>
   License: GNU GPL version 3
   Version: 3.0.1 (2021-11-22)
   Website: http://www.gingerblue.org/

 */

#ifndef _GINGERBLUE_OPERATOR_H_
#define _GINGERBLUE_OPERATOR_H_ 1

#define GINGERBLUE_OPERATOR 5000

typedef struct _GingerblueOperator {
  gboolean operator_logged_in;
  gchar *username;
  gchar *website;
  gchar *password;
} GingerblueOperator;

GingerblueOperator *gingerblue_operator_new(int logged_in, gchar *username, gchar *website, gchar *password);

#endif /* _GINGERBLUE_OPERATOR_H_ */
