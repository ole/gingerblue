/* $Id$

   Copyright (C) 2020-2022 Aamot Software
   Author(s): Ole Aamot <ole@gnome.org>
   License: GNU GPL version 3
   Version: 6.2.0 (2022-07-09)
   Website: http://www.gingerblue.org/

 */

#include <gtk/gtk.h>
#include <gst/gst.h>
#include "gingerblue.h"
#include "gingerblue-studio-config.h"

extern GtkWidget *computer_entry;
extern GtkWidget *studio_entry;

GtkWidget *gingerblue_main_loop (GingerblueData *gingerblue) {
	GingerblueData *Gingerblue = gingerblue;
	Gingerblue->window = main_studio_config (gtk_entry_get_text(GTK_ENTRY(studio_entry)), gtk_entry_get_text(GTK_ENTRY(computer_entry)));
	gtk_window_set_title (Gingerblue->window, g_strconcat(gtk_entry_get_text(GTK_ENTRY(computer_entry)), " on ", gtk_entry_get_text(GTK_ENTRY(studio_entry)), NULL));
        gtk_widget_show_all (Gingerblue->window);
}
